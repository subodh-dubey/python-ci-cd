FROM python:latest
# Install app dependencies

RUN dir
RUN mkdir /app2
COPY . /app2

RUN pip install -r /app2/requirement.txt
CMD ["python", "/app2/pgm1.py"]